module.exports = function(grunt) {
    grunt.initConfig({

        //this file loads package.json
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            deploy: [
                'deploy'
            ]
        },

        concat: {
            server: {
                src: [
                    'source/main.js',
                    'source/modules/**/*.js'
                ],
                dest: 'deploy/server.js'
            },
            client: {
                src: [
                    'source/public/**/*.js', //** means it will find any file in public that ends with .js'//
                    '!source/public/bower_components/**/*.js'
                ],
                dest: 'deploy/public/app.js'
            }
        },

        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            public: {
                files: {
                    'deploy/public/app.annotated.js': ['deploy/public/app.js']
                }
            }
        },
        uglify: {
            public: {
                options: {
                    mangle: true,
                    banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */'
                },
                src: [
                    'deploy/public/app.annotated.js'
                ],
                dest: 'deploy/public/app.min.js'
            },

            server: {
                options: {
                    mangle: true,
                    banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */'
                },
                src: [
                    'deploy/server.js'
                ],
                dest: 'deploy/server.min.js'
            }
        },

        jshint: {
            options: {
                reporter: require('jshint-stylish')
            },
            all: [
                'source/main.js',
                'source/public/**/*.js',
                'source/modules/**/*.js'
            ],
            node: ['deploy/server.js']
        },

        copy: {
            public: {
                expand: true,
                cwd: 'source/public',
                src: {
                    '*.html',
                    '*.css'

                },
                dest: 'deploy/public'
            },
            assets: {
                expand: true,
                cwd: 'source',
                src: [
                    'assets/**/*'
                ],
                dest: 'deploy'
            },

            bower: {
                expand: true,
                cwd: 'source',
                src: [
                    'bower_components/**/*'
                ],
                dest: 'deploy'
            }
        },

        processhtml: {
            public: {
                files: {
                    'deploy/public/index.html': ['deploy/public/index']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat'); //these must be in this order!
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-processhtml');
    grunt.loadNpmTasks('grunt-contrib-clean');


    grunt.registerTask('default', [
        'clean',
        'concat',
        'ngAnnotate',
        'jshint',
        'uglify'
        'copy',
        'processhtml'
    ]);

};

