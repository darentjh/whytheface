/**
 * Created by Daren on 19/4/16.
 */
var express = require("express");
var app = express();

var favicon = require('serve-favicon');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: false}));
var urlencodedParser = bodyParser.urlencoded({ extended: false });

var uuid = require('node-uuid');
var uuid1 = uuid.v1();

var session = require("express-session");

var handlebars = require("express-handlebars");
app.engine("handlebars", 
    handlebars({defaultLayout: "main",
        helpers:{
            isActive: function(view, actual){
                // console.info("view = %s, actual =%s", view, actual);
                if (view === actual)
                    return ("active");
                return ("");
            }
        }
    })
);
app.set("view engine", "handlebars");

var multer  = require('multer');
var upload = multer({
    dest: __dirname + "/assets"
});

app.use(favicon(__dirname + "/public/img/favicon.ico"));

var api_key = "key-2675229d8325be00bd3f86511233bde9";
var domain = 'mailgun.acuriouscuration.com';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
var data = {
    from: 'A Curious Curation <daren@acuriouscuration.com>',
    to: 'daren@acuriouscuration.com',
    subject: 'Hello',
    text: 'Testing some Mailgun awesomeness!'
};
mailgun.messages().send(data, function (error, body) {
    console.log(body);
});

var check = require("connect-ensure-login");

var count = 0;

var passport = require("passport");
var PassportLocal = require("passport-local").Strategy;
const local = new PassportLocal(
    {
        usernameField: "email",
        passwordField: "password"
    },
    function(username, password, done) {
        console.info("authenticating: %s, %s", username, password);
        
        if (username == "daren.tjh@gmail.com" && password == "12345678")
            return(done(null,
                {
                    loginId: username,
                    loginTime: new Date()
                }));
        return(done(null, false));
    }   
);

passport.serializeUser(function(loginInfo, done) {
    console.info("IN serializeUser: %s", JSON.stringify(loginInfo));
    done(null, loginInfo.loginId);
});

passport.deserializeUser(function(uid, done) {
    console.info("IN deserializeUser: %s", uid);
    //Now we use uid to pull user details from DB and enrich user obj (user obj is req.user)
    done(null, {
        username: uid,
        loginTime: new Date(),
        counter: count++
    });
});

passport.use(local);

//Process Request
app.use(session({
    secret: "pukcats",
    resave: false, //if session hasnt been modified, dont resave it (if cart has stuff but hasnt been changed)
    saveUninitialized: false //if session hasnt been modified, dont resave it( if cart doesnt have stuff, but not gonna add, dont save)
})); //session MUST come before app.use(protected)

app.use(passport.initialize());
app.use(passport.session());

//this checks if authentication is done
app.use("/state/*", 
    check.ensureLoggedIn("/login.html"),
    function(req, res, next) {
        console.info("User data: %s", JSON.stringify(req.user));
        next();
    }); //DO NOT PUT LOGIN PAGE IN PROTECTED!

app.post("/authenticate",
    passport.authenticate("local", {
        successReturnToOrRedirect: "/index.html#/listings",
        failureRedirect: "/incorrect_login.html"
    })
);

app.get("/logout", function(req, res) {
    req.logout();
    res.redirect("/login.html");
});

app.post("/upload-image",
    upload.single("product_image"),
        function(req, res) {
            connPool.getConnection(function (err, conn) {
                //If there are error, report it
                if (err) {
                    console.error("get connection error: %s", err);
                    res.render("error");
                    return;
                }
                try {
                    var paramInsert = "INSERT INTO vendorUpload values (?,?,?,?)";
                    conn.query(paramInsert,
                        [req.body.email, req.body.password, req.body.product_name, req.body.product_description],
                        function (err, rows) {
                            if (err) {
                                console.error("post has error: %s", err);
                                res.render("error");
                                return;
                            }
                            //Render the template with rows as the context
                            res.render("uploaded", {
                                productName:req.body.product_name,
                                productDesc: req.body.product_description,
                                productImage: req.file.filename,
                                type: req.file.mimetype
                            });
                        });
                } catch(ex) {
                    console.err("---> err: %s", ex);
                } finally {
                    //IMPORTANT! Release the connection
                    conn.release();
                }
            });
        });

var config = require("./modules/dbconfig");
var mysql = require("mysql");
var connPool = mysql.createPool(config);

app.get("/", function (req, res) {
    res.render("index");
});

app.get("/menu/:category", function(req, res) {
    var cat = req.params.category;
    switch (cat) {
        case "index": //if ((cat == "index") || (cat == "about") || (cat == "register"))
        case "aboutus":
        case "register":
        case "contactus":
        case "error":
        case "upload":
            res.render(cat);
            break;
        
        case "shop":
            //Get a connection from the pool
            connPool.getConnection(function(err, conn) {
                //If there are error, report it
                if (err) {
                    console.error("get connection error: %s", err);
                    res.render("/error");
                    return;
                }
                //Perform the query
                try {
                    conn.query("select * from vendorUpload", function (err, rows) {
                        if (err) {
                            console.error("query has error: %s", err);
                            res.render("/error");
                            return;
                        }
                        //Render the template with rows as the context
                        res.format( {
                            "text/html": function() {
                                res.render("shop", { vendorUpload: rows });
                            },
                            
                            "application/json": function() {
                                res.json(rows);
                            }
                        })

                    });
                } catch (ex) {
                    res.render("error", {error: ex});
                } finally {
                    //IMPORTANT! Release the connection
                    conn.release();
                }
            });
            break;
        
        default:
            res.redirect("/");
    }
});

app.get("/menu/shop/:product_id", function(req, res) {
    connPool.getConnection(function(err, conn) {
       try {
           conn.query("SELECT * from vendorUpload where product_id = ?", [req.params.product_id],
           function(err, rows) {
               if (err) {
                   console.error("Query has error: %s", err);
                   res.render("/error");
                   return;
               }
               
               if(rows.length)
                   res.json(rows[0]);
               else
                    console.error("Query has error: %s", err);
                    res.render("/error");
           });
       } catch(ex) {}
        finally {
           conn.release();
       }
    });
});


app.post('/register', function(req,res) {
    connPool.getConnection(function (err, conn) {
        //If there are error, report it
        if (err) {
            console.error("get connection error: %s", err);
            res.render("index");
            return;
        }
        try {
            var paramInsert = "INSERT INTO register values (?,?,?,?,?)";
            conn.query(paramInsert,
                [req.body.email,req.body.password,req.body.cfmpassword,req.body.firstname,req.body.lastname],
                function (err, rows) {
                    if (err) {
                        console.error("post has error: %s", err);
                        res.render("index");
                        return;
                    }
                    //Render the template with rows as the context
                    res.redirect("index");
                });
        } finally {
            //IMPORTANT! Release the connection
            conn.release();
        }
    });
});

app.get("/search", function(req, res) {
    console.info("productquery = %s", req.query.product_name);
    //Get a connection from the pool
    connPool.getConnection(function(err, conn) {
        //Check for error
        if (err) {
            res.render("error", { error: err});
            return;
        }
        //Construct our SQL
        var q = "select * from vendorUpload where product_name like " + '%s';
        var paramQuery = "select * from vendorUpload where product_name like ? ";
        console.info("query: %s", q);
        //Perform the query
        try {
            conn.query(paramQuery, // query
                ["%" + req.query.product_name + "%"], //actual parameters based on position
                function (err, rows) {
                    if (err) {
                        res.render("error", {error: err});
                        return;
                    }
                    res.render("shop", {vendorUpload: rows});
                });
        } catch (ex) {
            res.render("error", {error: ex});
        } finally {
            conn.release();
        }
    });
});



app.use(express.static(__dirname + "/public"));
app.use("/assets", express.static(__dirname + "/assets"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.use(function (req, res, next) {
    console.error("File not found: %s", req.originalUrl);
    res.redirect("/menu/error");
});

app.set("port", process.env.APP_PORT || 3000);

app.listen(app.get("port"), function () {
    console.info("WTF App is listening on Port %s", app.get("port"));
});