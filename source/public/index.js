// /**
//  * Created by Daren on 19/4/16.
//  */
// var cartApp = angular.module('cartApp', []);
//
// cartApp.config(function($interpolateProvider) {
//     $interpolateProvider.startSymbol('{[{');
//     $interpolateProvider.endSymbol('}]}');
// });
//
// cartApp.controller('cartCtrl', function () {
//     this.products = [
//         {'name': 'Product A', 
//             'description': 'THIS IS PRODUCT A LA'},
//         {'name': 'Product B',
//             'description': 'THIS IS PRODUCT B LA'},
//         {'name': 'Product B',
//             'description': 'THIS IS PRODUCT C LA'},
//         {'name': 'Product B',
//             'description': 'THIS IS PRODUCT D LA DUH'}
//     ];
// });
//


// always put your angular module in a IIFE
// this makes sure that we don't pollute global variables
(function() {
    var CatalogueApp = angular.module("CatalogueApp", ["ui.router"]);

    var ListingsCtrl = function($http, $state) {

        var vm = this;
        vm.shop = [];
        vm.gotoDetails = function(pid) {
            $state.go("details", 
                {product_id: pid}
            )
        };

        $http.get("/menu/shop")
            .then(function(result) {
                vm.shop = result.data;
            }).catch(function(err) {
            console.error(">>error: %s", err);
        });
        
    };

    var DetailsCtrl = function($http, $state, $stateParams) {
        var vm = this;
        vm.product = {};
        console.log($stateParams.product_id);
        
        $http.get("/menu/shop/" + $stateParams.product_id)
            .then(function(result) {

                vm.product = result.data
            }).catch(function(err) {
                console.error(">> error: $s", err);
        });
    };
    
    

    var CatalogueConfig = function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("listings", {
                url: "/listings",
                templateUrl: "/state/listings.html",
                controller: ["$http", "$state", ListingsCtrl],
                controllerAs: "listingsCtrl"
                
            }).state("details", {
                url:"/product/:product_id",
                templateUrl: "state/details.html",
                controller: ["$http", "$state", "$stateParams", DetailsCtrl],
                controllerAs: "detailsCtrl"
            });

        $urlRouterProvider.otherwise("/listings");
    };
    
    var CatalogueCtrl = function() {};

    CatalogueApp.config(["$stateProvider", "$urlRouterProvider", CatalogueConfig]);
    
    CatalogueApp.controller("CatalogueCtrl", [CatalogueCtrl]);

})();

